<?php

/**
 * @file
 * The new currency of the Republic of Belarus.
 */

/**
 * Implements hook_menu().
 */
function commerce_currency_byn_menu() {
  $items = array();
  $items['admin/commerce/config/convert_byr2byn'] = array(
    'title' => 'Convert BYR to BIN',
    'page callback' => array('commerce_currency_byn_convert'),
    'access arguments' => array('configure store'),
  );
  return $items;
}

/**
 * Implements hook_commerce_currency_info().
 */
function commerce_currency_byn_commerce_currency_info() {
  return array(
    'BYN' => array(
      'code' => 'BYN',
      'numeric_code' => '933',
      'symbol' => 'руб.',
      'name' => t('Belarusian ruble new'),
      'symbol_placement' => 'after',
      'code_placement' => 'hidden',
      'minor_unit' => t('Kopek'),
      'major_unit' => t('Ruble'),
      'rounding_step' => '0',
      'thousands_separator' => ' ',
      'decimal_separator' => ',',
    ),
  );
}

/**
 * Start batch process for convert.
 */
function commerce_currency_byn_convert() {
  $batch = array(
    'title' => t('Converting currencies'),
    'operations' => array(
      array('commerce_currency_byn_convert_batch_operation', array()),
    ),
    'finished' => 'commerce_currency_byn_convert_batch_finished',
  );
  batch_set($batch);
  batch_process('admin/reports/status');
}

/**
 * Batch process operation.
 *
 * Find all products with old currency, change amount and currency,
 * save changed product.
 *
 * @param array $context
 *    Batch context.
 */
function commerce_currency_byn_convert_batch_operation(&$context) {
  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = db_query('SELECT COUNT(DISTINCT `entity_id`) FROM {field_data_commerce_price} WHERE `commerce_price_currency_code` = :code', array(':code' => 'BYR'))->fetchField();
  }
  $limit = 5;
  $result = db_select('field_data_commerce_price')
    ->fields('field_data_commerce_price', array('entity_id'))
    ->condition('commerce_price_currency_code', 'BYR')
    ->orderBy('entity_id')
    ->range(0, $limit)
    ->execute();
  foreach ($result as $row) {
    $product = entity_metadata_wrapper('commerce_product', commerce_product_load($row->entity_id));
    $product->commerce_price->amount = ceil($product->commerce_price->amount->value() / 100);
    $product->commerce_price->currency_code = 'BYN';
    $product->save();
    $context['sandbox']['progress']++;
    $context['message'] = $product->title->value(array('sanitize' => TRUE));
  }
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Batch process finished.
 *
 * @param string $success
 *    Success message.
 * @param array $results
 *    Array with results.
 * @param array $operations
 *    Array of operations.
 */
function commerce_currency_byn_convert_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = t('Finished successfully.');
  }
  else {
    $message = t('Finished with an error.');
  }
  drupal_set_message($message);
}
