### Summary

In 2016, the denomination of the national currency will be held in Belarus
in connection with the change than the international designation
of the Belarusian ruble. Relevant amendments recently issued Agency for support
of the international standard ISO 4217 "Codes for the representation
of currencies and funds". From now on, the ruble will alphabetic code BYN.
BY - country code, and N - from the word "new".

The module provides not only an opportunity to select a new currency and
the conversion of the old currency to the new currency.

To convert currency open page
http://example.com/admin/commerce/config/convert_byr2byn
(Store -> Configuration -> Convert BYR to BIN)


### Requirements

None


### Installation

Install as usual, see http://drupal.org/node/895232 for further information.


### Contact

Current maintainers:
* Denis Nikiforov (WirGen) - https://www.drupal.org/user/1573728
